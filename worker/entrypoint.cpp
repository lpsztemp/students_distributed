#include "interface.h"
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#pragma comment(lib, "Ws2_32.lib")

#include <stdio.h>

#define FAIL_EXIT(err) do {printf("Error %u occurred at %d.\n", WSAGetLastError(), __LINE__); exit_code = (err); goto failure;} while(false)

int read_data(SOCKET sock, void* dest, int bytes);

void add_map_proc(SOCKET client_socket)
{
	word result = 0.0;
	uint32_t size;
	word *V = nullptr;
	int errc = read_data(client_socket, (char*) &size, 4);
	if (errc != 4)
		goto stop;
	V = (word*) malloc(size * sizeof(word));
	if (!V)
		goto stop;
	errc = read_data(client_socket, (char*) V, size * sizeof(word));
	if (errc != size * sizeof(word))
		goto stop;

	for (uint32_t i = 0; i < size; ++i)
		result += V[i];
	errc = send(client_socket, (char*) &result, sizeof(word), 0);
	if (errc != sizeof(double))
		goto stop;
stop:
	if (V)
		free(V);
	shutdown(client_socket, SD_BOTH);
	closesocket(client_socket);
}

void add_reduce_proc(SOCKET client_socket)
{
	word result, x, y;
	int errc = read_data(client_socket, (char*) &x, sizeof(word));
	if (errc != sizeof(word))
		goto stop;
	errc = read_data(client_socket, (char*) &y, sizeof(word));
	if (errc != sizeof(word))
		goto stop;
	result = x + y;
	errc = send(client_socket, (char*) &result, sizeof(word), 0);
	if (errc != sizeof(double))
		goto stop;
stop:
	shutdown(client_socket, SD_BOTH);
	closesocket(client_socket);
}

std::string get_terminated_string(SOCKET sock)
{
	std::string result;
	char buf;
	while (true)
	{
		recv(sock, &buf, 1, 0);
		if (buf == 0)
			break;
		result.append(1, buf);
	}
	return result;
}

unsigned stats[256];
void get_resource_stats(SOCKET sock)
{
	std::string server = get_terminated_string(sock);
	std::string service = get_terminated_string(sock);
	std::string resource = get_terminated_string(sock);
	unsigned B, E;
	read_data(sock, &B, 4);
	read_data(sock, &E, 4);
	shutdown(sock, SD_BOTH);
	closesocket(sock);
	std::string data = get_content_from(server.data(), service.data(), resource.data());
	memset(stats, 0, 256 * 4);
	for (size_t i = B; i < E; ++i)
		stats[data[i]]++;
}

void get_resource_stats_results(SOCKET sock)
{
	send(sock, (char*) stats, 256 * 4, 0);
}

void msg_proc(SOCKET client_socket)
{
	uint32_t msg;
	int errc = read_data(client_socket, (char*) &msg, 4);
	if (errc != 4)
	{
		closesocket(client_socket);
		return;
	}
	if (msg == MSG_ADD_MAP)
		add_map_proc(client_socket);
	else if (msg == MSG_ADD_REDUCE)
		add_reduce_proc(client_socket);
	else if (msg == MSG_GET_RESOURCE_STATS)
		get_resource_stats(client_socket);
	else if (msg == MSG_GET_RESOURCE_STATS_RESULT)
		get_resource_stats_results(client_socket);
	else
	{
		//send(client_socket, -1, 4, 0);  TCP is reliable. Invalid message => DoS.
		closesocket(client_socket);
		return;
	}
}

int main(int argc, char** argv)
{
	WSADATA wsa_data;
	SOCKET listen_socket = INVALID_SOCKET, client_socket = INVALID_SOCKET;
	int exit_code = 0;
	int errc;
	unsigned last_bytes_count = 0;
	sockaddr_in client_addr;
	int client_addr_size = sizeof(sockaddr_in);
	sockaddr_in addr;

	errc = WSAStartup(MAKEWORD(2, 2), &wsa_data);
	if (errc != 0)
		return __LINE__;
	listen_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (listen_socket == INVALID_SOCKET)
		FAIL_EXIT(__LINE__);
	memset(&addr, 0, sizeof(sockaddr_in));
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = INADDR_ANY;
	addr.sin_port = htons(WORKER_PORT);
	errc = bind(listen_socket, (sockaddr*) &addr, sizeof(sockaddr_in));
	if (errc != 0)
		FAIL_EXIT(__LINE__);
	errc = listen(listen_socket, 1);
	if (errc != 0)
		FAIL_EXIT(__LINE__);
	printf("Awaiting connection.\n");
	while (true)
	{
		client_socket = accept(listen_socket, (sockaddr*) &client_addr, &client_addr_size);
		if (client_socket == INVALID_SOCKET)
		{
			errc = WSAGetLastError();
			FAIL_EXIT(__LINE__);
		}
		if (client_addr_size != sizeof(sockaddr_in) || client_addr.sin_family != AF_INET)
		{
			printf("Client connection is improper.\n");
			FAIL_EXIT(__LINE__);
		}else
		{
			char client_addr_string[49];
			printf("A TCP/IPv4 client %s:%u has connected.\n", inet_ntop(AF_INET, &client_addr.sin_addr, client_addr_string, client_addr_size), ntohs(client_addr.sin_port));
		}

		msg_proc(client_socket);

	}
	closesocket(listen_socket);
	listen_socket = INVALID_SOCKET;
failure:
	if (client_socket != INVALID_SOCKET)
		closesocket(client_socket);
	if (listen_socket != INVALID_SOCKET)
		closesocket(listen_socket);
	WSACleanup();
	return exit_code;
}