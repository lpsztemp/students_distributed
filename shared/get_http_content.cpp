#include "interface.h"

#define FAIL_EXIT(err) do {fprintf(stderr, "FAIL_EXIT invoked at line %u of %s with error code %u\n", __LINE__, __FILE__, (err)); exit(__LINE__);} while(false)
#define FAIL_ON_SOCKET_ERROR(x) do {if ((x) == SOCKET_ERROR) FAIL_EXIT(WSAGetLastError());} while (false)

static inline void send_string(SOCKET sock, const char* str)
{
	send(sock, str, (int) strlen(str), 0);
}

static std::string read_http_headers(SOCKET sock) //read start-line and headers
{
	char crlf_buf[3] = "\r\n";
	int crlf_seq = 0;
	std::string start_line, result;
	char buf;
	while (true)
	{
		int err = recv(sock, &buf, 1, 0);
		FAIL_ON_SOCKET_ERROR(err);
		if (err != 1)
			FAIL_EXIT(-1);
		start_line.append(1u, buf);
		if (buf != crlf_buf[crlf_seq & 1])
			crlf_seq = 0;
		else if (++crlf_seq == 2)
			break;
	} 
	if (start_line != "HTTP/1.1 200 OK\r\n")
		FAIL_EXIT(-1);
	crlf_seq = 0;
	while (true)
	{
		int err = recv(sock, &buf, 1, 0);
		FAIL_ON_SOCKET_ERROR(err);
		if (err != 1)
			std::exit(__LINE__);
		result.append(1u, buf);
		if (buf != crlf_buf[crlf_seq & 1])
			crlf_seq = 0;
		else if (++crlf_seq == 4)
			break;
	}
	return result;
}
static size_t parse_content_length(const char* headers)
{
	const char* ptr = headers;
	const char content_length_header[] = "content-length:";
	while (*ptr != 0)
	{
		if (tolower(*ptr) == 'c')
		{
			unsigned content_length = 0;
			size_t i = 1;
			for (i; i < sizeof(content_length_header) - 1; ++i)
			{
				if (tolower(ptr[i]) != content_length_header[i])
					break;
			}
			if (i == sizeof(content_length_header) - 1)
			{
				ptr += sizeof(content_length_header) - 1;
				while (*ptr == ' ')
					++ptr;
				if (*ptr == 0)
					FAIL_EXIT(-1);
				while (*ptr != '\r')
				{
					if (*ptr < '0' || *ptr > '9')
						FAIL_EXIT(-1);
					content_length *= 10;
					content_length += *ptr++ - '0';
				}
				if (*++ptr != '\n')
					FAIL_EXIT(-1);
				return content_length;
			}
		}
		++ptr;
	}
	return 0;
}

std::size_t get_content_length_from(const char* addr, const char* service, const char* resource)
{
	SOCKET sock = connect_to(addr, service);
	if (sock == INVALID_SOCKET)
		std::exit(-1);
	send_string(sock, "HEAD ");
	send_string(sock, resource);
	send_string(sock, " HTTP/1.1\r\n");
	send_string(sock, "Accept: text/*\r\n");
	send_string(sock, "Host: "); send_string(sock, addr); send_string(sock, "\r\n");
	send_string(sock, "\r\n");
	shutdown(sock, SD_SEND);
	std::string headers = read_http_headers(sock);
	shutdown(sock, SD_RECEIVE);
	return parse_content_length(headers.data());
}

std::string get_content_from(const char* addr, const char* service, const char* resource)
{
	SOCKET sock = connect_to(addr, service);
	if (sock == INVALID_SOCKET)
		std::exit(-1);
	send_string(sock, "GET ");
	send_string(sock, resource);
	send_string(sock, " HTTP/1.1\r\n");
	send_string(sock, "Accept: text/*\r\n");
	send_string(sock, "Host: "); send_string(sock, addr); send_string(sock, "\r\n");
	send_string(sock, "\r\n");
	shutdown(sock, SD_SEND);
	std::string headers = read_http_headers(sock);
	std::size_t content_length = parse_content_length(headers.data());
	std::string payload;
	while (content_length > 0)
	{
		char buf;
		int err = recv(sock, &buf, 1, 0);
		FAIL_ON_SOCKET_ERROR(err);
		if (err != 1)
			FAIL_EXIT(-1);
		payload.append(1u, buf);
		--content_length;
	}
	shutdown(sock, SD_RECEIVE);
	closesocket(sock);
	return payload;
}