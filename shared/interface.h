#define WORKER_PORT 20000

#define MAKE_STRING_(X) #X
#define MAKE_STRING(X) MAKE_STRING_(X)

typedef double word;

//Input: size in words, 4 bytes | V, size1 words
//Output: sum, 1 word
#define MSG_ADD_MAP 0x00000001
//Input: left addend, 1 word | right addend, word
//Output: sum, 1 word
#define MSG_ADD_REDUCE 0x00000002
//Input: server, 0, service, 0, resource, 0, interval_begin (4b), interval_end (4b)
//Output:
#define MSG_GET_RESOURCE_STATS 0x00000012
//Input: 
//Output: stats, 256 4-byte words
#define MSG_GET_RESOURCE_STATS_RESULT 0x00000013


#include <winsock2.h>
#include <ws2tcpip.h>
int read_data(SOCKET sock, void* dest, int bytes);
SOCKET connect_to(const char* addr, const char* service);

#include <stdlib.h>
size_t get_content_length_from(const char* addr, const char* service, const char* resource);
#include <string>
std::string get_content_from(const char* addr, const char* service, const char* resource);