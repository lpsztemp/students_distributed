#include "interface.h"

#define FAIL_EXIT(err) do {fprintf(stderr, "FAIL_EXIT invoked at line %u of %s with error code %u\n", __LINE__, __FILE__, (err)); exit(__LINE__);} while(false)
#define FAIL_ON_SOCKET_ERROR(x) do {if ((x) == SOCKET_ERROR) FAIL_EXIT(WSAGetLastError());} while (false)

SOCKET connect_to(const char* addr, const char* service)
{
	SOCKET connect_socket = INVALID_SOCKET;
	addrinfo hints, *peer = nullptr;
	memset(&hints, 0, sizeof(addrinfo));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	if (getaddrinfo(addr, service, &hints, &peer) != 0)
		return INVALID_SOCKET;
	for(addrinfo* ptr=peer; ptr != nullptr; ptr=ptr->ai_next)
	{
		connect_socket = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);
		if (connect_socket == INVALID_SOCKET)
			break;
		int errc = connect(connect_socket, ptr->ai_addr, (int) ptr->ai_addrlen);
		if (errc != 0)
		{
			closesocket(connect_socket);
			connect_socket = INVALID_SOCKET;
		}else
			break;
	}
	freeaddrinfo(peer);
	return connect_socket;
}
