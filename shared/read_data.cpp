#include <winsock2.h>
#include <ws2tcpip.h>

int read_data(SOCKET sock, void* dest, int bytes)
{
	char* dest_bytes = (char*) dest;
	int packet_size = 0;
	for (int received_total = 0; received_total < bytes; received_total += packet_size)
	{
		packet_size = recv(sock, &dest_bytes[received_total], bytes - received_total, 0);
		if (!packet_size) //SHUTDOWN
			return received_total;
		else if (packet_size == SOCKET_ERROR)
			return SOCKET_ERROR;
	}
	return bytes;
}
