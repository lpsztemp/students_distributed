#include "interface.h"

#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <string.h>
#pragma comment(lib, "Ws2_32.lib")

#include <stdio.h>
#include <stdint.h>

#define FAIL_EXIT(err) do {exit_code = (err); goto failure;} while(false)
int read_data(SOCKET sock, void* dest, int bytes);

SOCKET* connect_any_workers(const char** worker_addresses, size_t worker_count, size_t* connection_count)
{
	SOCKET* workers = (SOCKET*) malloc(worker_count * sizeof(SOCKET));
	size_t connections = 0;
	if (!workers)
	{
		fprintf(stderr, "Failed to allocate %u bytes of memory.\n", (unsigned) (worker_count * sizeof(SOCKET)));
		exit(-1);
	}
	for (size_t i = 0; i < worker_count; ++i)
	{
		workers[connections] = connect_to(worker_addresses[i], MAKE_STRING(WORKER_PORT));
		if (workers[connections] != INVALID_SOCKET)
			connections++;
	}
	*connection_count = connections;
	if (!connections)
	{
		free(workers);
		return nullptr;
	}
	return workers;
}

void close_connections(SOCKET* workers, size_t connections)
{
	for (size_t t = 0; t < connections; ++t)
	{
		shutdown(workers[t], SD_BOTH);
		closesocket(workers[t]);
	}
	free(workers);
}

double add_proc(const char** worker_addresses, size_t worker_count, double* vector, size_t vector_size)
{
	double* maps;
	size_t connections, reduce_size;
	SOCKET* workers;
	//Map - request
	workers = connect_any_workers(worker_addresses, worker_count, &connections);
	if (!workers)
	{
		fprintf(stderr, "Failed to connect to any workers.\n");
		exit(-1);
	}
	for (size_t t = 0; t < connections; ++t)
	{
		uint32_t msg = MSG_ADD_MAP;
		if (send(workers[t], (char*) &msg, 4, 0) != 4)
		{
			fprintf(stderr, "Failed to send to a worker.\n");
			exit(-1);
		}
		size_t off = vector_size % connections, size = vector_size / connections;
		if (t < off)
			off = ++size * t;
		else
			off += size * t;
		if (send(workers[t], (char*) &size, 4, 0) != 4)
		{
			fprintf(stderr, "Failed to send to a worker.\n");
			exit(-1);
		}
		if ((size_t) send(workers[t], (char*) &vector[off], (int) (size * sizeof(double)), 0) != size * sizeof(double))
		{
			fprintf(stderr, "Failed to send to a worker.\n");
			exit(-1);
		}
	}
	reduce_size = connections;
	maps = (double*) malloc(reduce_size * sizeof(double));
	if (!maps)
	{
		fprintf(stderr, "Failed to allocate %u bytes of memory.\n", (unsigned) (reduce_size * sizeof(double)));
		exit(-1);
	}
	//Map - response
	for (size_t t = 0; t < connections; ++t)
	{
		int errc = read_data(workers[t], (char*) &maps[t], sizeof(double));
		if (errc != sizeof(double))
		{
			fprintf(stderr, "Failed to recv from a worker.\n");
			exit(-1);
		}
	}
	//close connection for map
	close_connections(workers, connections);

	//Reduce
	for (size_t k = 1, s = 2; k < reduce_size; k = s, s += s)
	{
		//Connect again
		workers = connect_any_workers(worker_addresses, reduce_size, &connections);
		if (!workers)
		{
			fprintf(stderr, "Failed to connect to any workers.\n");
			exit(-1);
		}
		if (connections != reduce_size)
		{
			close_connections(workers, connections);
			fprintf(stderr, "Failed to connect to neccessary number of workers.\n");
			exit(-1);
		}
		//Requests
		for (size_t t = 0; t + k < reduce_size; t += s)
		{
			uint32_t msg = MSG_ADD_REDUCE;
			if (send(workers[t], (char*) &msg, 4, 0) != 4)
			{
				fprintf(stderr, "Failed to send to a worker.\n");
				exit(-1);
			}
			if (send(workers[t], (char*) &maps[t], sizeof(double), 0) != sizeof(double))
			{
				fprintf(stderr, "Failed to send to a worker.\n");
				exit(-1);
			}
			if (send(workers[t], (char*) &maps[t + k], sizeof(double), 0) != sizeof(double))
			{
				fprintf(stderr, "Failed to send to a worker.\n");
				exit(-1);
			}
		}
		//Responses
		for (size_t t = 0; t + k < reduce_size; t += s)
		{
			if (read_data(workers[t], (char*) &maps[t], sizeof(double)) != sizeof(double))
			{
				fprintf(stderr, "Failed to recv from a worker.\n");
				exit(-1);
			}
		}
		close_connections(workers, connections);
	}
	return maps[0];
}

void send_string(SOCKET sock, const char* str)
{
	send(sock, str, (char) strlen(str), 0);
}

void send_int(SOCKET sock, unsigned val)
{
	send(sock, (char*) &val, 4, 0);
}

unsigned result[256];
void gather_statistics(const char** worker_addresses, size_t worker_count)
{
	size_t N = get_content_length_from("norvig.com", "80", "/big.txt");
	size_t T = worker_count;
	for (size_t t = 0; t < T; ++t)
	{
		size_t Nt = N / T;
		size_t Bt, Et;
		if (t < N % T)
			Bt = ++Nt * t;
		else
			Bt = Nt * t + N % T;
		Et = Bt + Nt;
		SOCKET worker = connect_to(worker_addresses[t], MAKE_STRING(WORKER_PORT));
		send_int(worker, MSG_GET_RESOURCE_STATS);
		send_string(worker, "norvig.com");
		char null = 0;
		send(worker, &null, 1, 0);
		send_string(worker, "80");
		send(worker, &null, 1, 0);
		send_string(worker, "/big.txt");
		send(worker, &null, 1, 0);
		send(worker, (char*) &Bt, 4, 0);
		send(worker, (char*) &Et, 4, 0);
		shutdown(worker, SD_BOTH);
		closesocket(worker);
	}
	memset(result, 0, sizeof(result));
	for (size_t t = 0; t < T; ++t)
	{
		unsigned worker_result[256];
		SOCKET worker = connect_to(worker_addresses[t], MAKE_STRING(WORKER_PORT));
		send_int(worker, MSG_GET_RESOURCE_STATS_RESULT);
		read_data(worker, worker_result, 256 * 4);
		shutdown(worker, SD_RECEIVE);
		closesocket(worker);
		for (size_t i = 0; i < 256; ++i)
			result[i] += worker_result[i];
	}
}

const char* worker_addresses[] =
{
	"127.0.0.1"
};
#define number_of_workers (sizeof(worker_addresses) / sizeof(worker_addresses[0]))

int main(int argc, char** argv)
{
	size_t n = 1027;
	double sum, *V;

	WSADATA wsa_data;
	int errc;
	int exit_code = 0;
	SOCKET connect_socket = INVALID_SOCKET;

	errc = WSAStartup(2 << 8 | 2, &wsa_data);
	if (errc != 0)
		return -1;

	V = (double*) malloc(n * sizeof(double));
	if (!V)
		goto failure;
	for (size_t i = 0; i < n; ++i)
		V[i] = (double) (i + 1);
	sum = add_proc(worker_addresses, number_of_workers, V, n);
	printf("Sum: %g.\n", sum);
	gather_statistics(worker_addresses, number_of_workers);
	printf("Symbol Code Count\n");
	for (unsigned i = 0; i < 256; ++i)
	{
		if (isprint(i))
			printf("\'%c\' %4d %u\n", (char) i, i, result[i]);
		else
			printf("    %4d %u\n", i, result[i]);
	}
failure:
	free(V);
	WSACleanup();
	return exit_code;
}